package org;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;

public class AppMain {

    public static void main(String[] args) throws Exception {
        new AppMain().run(args);
    }

    public void run(String[] args) throws Exception {
        InputStream in = AppMain.class.getResourceAsStream("/mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        SqlSession session = factory.openSession();

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("num01", 123.45);
        MyMapper mapper = session.getMapper(MyMapper.class);
        List<Map<String, Object>> result = mapper.selList01(param);
        result.forEach(row -> {
            System.out.println("---------------");
            row.forEach((columnName, value) -> {
                System.out.printf("columnName=%s, value=%s%n", columnName, value);
            });
        });
    }

}
