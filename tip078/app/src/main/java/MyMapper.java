package org;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

public interface MyMapper {

    @Select("select * from tip033 where num01 = #{param.num01}")
    public List<Map<String, Object>> selList01(@Param("param") Map param);

}