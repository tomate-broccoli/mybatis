# README.md

## sqlite3

- sqlite sample.db
- .schema sample
- create table tab01  ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255), "age" integer, "birthday" date, "created_at" datetime, "updated_at" datetime);
- insert into tab01 values(1, 'foo bar', current_date, current_date, current_date);
- .q

## gradle run
$ gradle run
Picked up JAVA_TOOL_OPTIONS:  -Xmx3489m

> Task :app:run
Picked up JAVA_TOOL_OPTIONS:  -Xmx3489m
---------------
columnName=birthday, value=2023-01-09
columnName=updated_at, value=2023-01-09
columnName=name, value=foo bar
columnName=created_at, value=2023-01-09
columnName=id, value=1
columnName=age, value=21
** SQL: select * from tab01 where id = ?
** id: null
