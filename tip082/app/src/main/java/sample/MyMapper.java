package sample;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

// @Mapper
public interface MyMapper {

    @Select("select * from tab01")
    public List<Map<String, Object>> selectAll();

    @Select("select * from tab01 where id = #{id}")
    public List<Map<String, Object>> selectList(@Param("id") int id);

    @Select("select * from tab01 where ${hoge}")
    public List<Map<String, Object>> selectList2(@Param("hoge") String hoge, @Param("id") int id);

}