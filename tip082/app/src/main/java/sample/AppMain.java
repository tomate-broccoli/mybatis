package sample;

import java.io.InputStream;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;

public class AppMain {

    public static void main(String[] args) throws Exception {
        try (InputStream in = App.class.getResourceAsStream("/mybatis-config.xml")) {
            SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
            factory.getConfiguration().addMapper(MyMapper.class);
    
            try (SqlSession session = factory.openSession()) {
                MyMapper mapper = session.getMapper(MyMapper.class);
                // List<Map<String, Object>> result = mapper.selectList(1);
                List<Map<String, Object>> result = mapper.selectList2(" 1 = 1 and id = #{id}", 1);
                result.forEach(row -> {
                    System.out.println("---------------");
                    row.forEach((columnName, value) -> {
                        System.out.printf("columnName=%s, value=%s%n", columnName, value);
                    });
                });

                Configuration configuration = session.getConfiguration();
                MappedStatement ms = configuration.getMappedStatement("sample.MyMapper.selectList2");
                BoundSql boundSql = ms.getBoundSql(new HashMap(){{
                    put("hoge", "1=1 and id = #{id}");
                    put("id", 1);
                }});
                System.out.println("** SQL: " + boundSql.getSql()); 

                List<ParameterMapping> boundParams = boundSql.getParameterMappings();
                for(ParameterMapping param : boundParams) {
                    String prop = param.getProperty();
                    System.out.println("** " + prop + ": "+ boundSql.getAdditionalParameter(prop));
                }
                //
                boundSql.getAdditionalParameters().entrySet().stream().forEach(e->System.out.println(e.getKey() +":"+ e.getValue()));
            }
        }
    }

}
