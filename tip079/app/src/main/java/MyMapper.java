package org;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

public interface MyMapper {

    @SelectProvider(type = MyProvider.class, method = "selList01")
    public List<Map<String, Object>> selList01(Map param);

    class MyProvider {
        public String selList01(Map map){
            StringBuffer buf = new StringBuffer("select * from tip033 ");
            if(map.get("num01") != null) buf.append("where num01 = #{num01} ");
            return buf.toString();
        }
    }

}