package tip087;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface MainMapper{
    public MyEntity selectByInt01(@Param("INT01") Boolean bool);
}
