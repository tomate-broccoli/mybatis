package tip087;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class MainService{
    @Autowired
    MainMapper mapper;

    public MyEntity selectByInt01(Boolean bool){
        return mapper.selectByInt01(bool);
    }
}
