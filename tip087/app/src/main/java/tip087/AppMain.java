package tip087;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.boot.SpringApplication;

@Component
public class AppMain {
    @Autowired
    MainController mainController;

    public static void main(String[] args) {
        try(ConfigurableApplicationContext ctx = SpringApplication.run(AppConfig.class, args)){
            AppMain main = ctx.getBean(AppMain.class);
            main.run(args);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    void run(String[] args){
        mainController.run(args);
    }
}
