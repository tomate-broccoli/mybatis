package tip087;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class MainController {
    @Autowired
    MainService mainService;

    public void run(String[] args){
        MyEntity entity = mainService.selectByInt01(true);
        System.out.println(entity.num01);
    }
}
