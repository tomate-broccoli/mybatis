package tip087;

import java.util.Date;
import lombok.Data;

@Data
public class MyEntity{
    public Integer num01;
    public String chr01;
    public Date dat01;
}
