package tip033;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

// @Mapper
public interface MyMapper {

    @Select("select * from tip033")
    public List<Map<String, Object>> selectAll();

    @Select("select * from tip033 where id = #{id}")
    public List<Map<String, Object>> selectList(@Param("id") int id);

}