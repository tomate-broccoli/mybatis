package tip033;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;

public class AppMain {

    public static void main(String[] args) throws Exception {
        try (InputStream in = App.class.getResourceAsStream("/mybatis-config.xml")) {
            SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
            factory.getConfiguration().addMapper(MyMapper.class);
    
            try (SqlSession session = factory.openSession()) {
                MyMapper mapper = session.getMapper(MyMapper.class);
                List<Map<String, Object>> result = mapper.selectList(1);
                result.forEach(row -> {
                    System.out.println("---------------");
                    row.forEach((columnName, value) -> {
                        System.out.printf("columnName=%s, value=%s%n", columnName, value);
                    });
                });

                Configuration configuration = session.getConfiguration();
                MappedStatement ms = configuration.getMappedStatement("tip033.MyMapper.selectList");
                BoundSql boundSql = ms.getBoundSql(null);
                System.out.println("** SQL: " + boundSql.getSql()); 

                List<ParameterMapping> boundParams = boundSql.getParameterMappings();
                for(ParameterMapping param : boundParams) {
                    String prop = param.getProperty();
                    System.out.println("** " + prop + ": "+ boundSql.getAdditionalParameter(prop));
                }
            }
        }
    }

}
